﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using WebApp3.Dao;

namespace WebApp3.Services
{
    public class DatabaseService
    {
        public List<UserDataView> getAllUsers()
        {
            using (MinetesterAmpersandEntities db = new MinetesterAmpersandEntities())
            {
                return db.UserDataViews.ToList();
            }
        }
        public System.Data.Entity.DbSet<UserDataView> getAllUsersDbSet()
        {
            using (MinetesterAmpersandEntities db = new MinetesterAmpersandEntities())
            {
                return db.UserDataViews;
            }
        }
        public UserDataView getUsers(String userName)
        {
            using (MinetesterAmpersandEntities db = new MinetesterAmpersandEntities())
            {
                return db.UserDataViews.FirstOrDefault(x => x.Username == userName);
            }
        }
    }
}