﻿using MvcContrib.Pagination;
using MvcContrib.UI.Grid;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApp3.Dao;
using WebApp3.Services;

namespace WebApp3.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Leaderboard(GridSortOptions sort,int? page)
        {
            ViewBag.Title = "Leader Board";
            IOrderedQueryable<UserDataView> query = null;
            if (sort.Column == null)
            {
                sort.Column = "GameCount";
                sort.Direction = MvcContrib.Sorting.SortDirection.Descending;
            }
            ViewData["sort"] = sort;
            MinetesterAmpersandEntities db = new MinetesterAmpersandEntities();
            var dbSet = db.UserDataViews.Where(x => !string.IsNullOrEmpty(x.Username));
            switch (sort.Column)
            {
                case "Name":
                    if (sort.Direction.ToString().ToUpper() == "DESCENDING")
                        query = dbSet.OrderByDescending(x => x.Username);
                    else
                        query = dbSet.OrderBy(x => x.Username);
                    break;
                case "GameCount":
                    if (sort.Direction.ToString().ToUpper() == "DESCENDING")
                        query = dbSet.OrderByDescending(x => x.GameCount);
                    else
                        query = dbSet.OrderBy(x => x.GameCount);
                    break;
                case "WinCount":
                    if (sort.Direction.ToString().ToUpper() == "DESCENDING")
                        query = dbSet.OrderByDescending(x => x.WinCount);
                    else
                        query = dbSet.OrderBy(x => x.WinCount);
                    break;
                case "Draw":
                    if (sort.Direction.ToString().ToUpper() == "DESCENDING")
                        query = dbSet.OrderByDescending(x => x.Draw);
                    else
                        query = dbSet.OrderBy(x => x.Draw);
                    break;
                case "Losses":
                    if (sort.Direction.ToString().ToUpper() == "DESCENDING")
                        query = dbSet.OrderByDescending(x => x.Losses);
                    else
                        query = dbSet.OrderBy(x => x.Losses);
                    break;
                case "PlayerScore":
                    if (sort.Direction.ToString().ToUpper() == "DESCENDING")
                        query = dbSet.OrderByDescending(x => x.PlayerScore);
                    else
                        query = dbSet.OrderBy(x => x.PlayerScore);
                    break;
                case "AmpersandScore":
                    if (sort.Direction.ToString().ToUpper() == "DESCENDING")
                        query = dbSet.OrderByDescending(x => x.AmpersandScore);
                    else
                        query = dbSet.OrderBy(x => x.AmpersandScore);
                    break;
                case "NumberGuesses":
                    if (sort.Direction.ToString().ToUpper() == "DESCENDING")
                        query = dbSet.OrderByDescending(x => x.NumberGuesses);
                    else
                        query = dbSet.OrderBy(x => x.NumberGuesses);
                    break;
                case "NumberPossibilities":
                    if (sort.Direction.ToString().ToUpper() == "DESCENDING")
                        query = dbSet.OrderByDescending(x => x.NumberPossibilities);
                    else
                        query = dbSet.OrderBy(x => x.NumberPossibilities);
                    break;
                case "Hit Percentage":
                    if (sort.Direction.ToString().ToUpper() == "DESCENDING")
                        query = dbSet.OrderByDescending(x => x.HitPercentage);
                    else
                        query = dbSet.OrderBy(x => x.HitPercentage);
                    break;
                case "Average Game Size":
                    if (sort.Direction.ToString().ToUpper() == "DESCENDING")
                        query = dbSet.OrderByDescending(x => x.AverageGameSize);
                    else
                        query = dbSet.OrderBy(x => x.AverageGameSize);
                    break;
            }
            int pageSize = 50;
            IPagination pagination = query.AsPagination(page ?? 1, pageSize);
            ViewBag.PlayerData = query.Skip(((page ?? 1) - 1) * pageSize).Take(pageSize).ToList();
            return View(pagination); 
        }
        [Route("PlayerInfo/(id)")]
        public ActionResult PlayerInfo(string id)
        {
            DatabaseService databaseService = new DatabaseService();
            UserDataView player = databaseService.getUsers(id);
            if (player == null)
            {
                ViewBag.PlayerName = $"Player {id} not found!";
                ViewBag.Player = new UserDataView();
                ViewBag.Title = $"Player {id} not found";
                return View();
            }
            ViewBag.Player = player;
            ViewBag.PlayerName = player.Username;
            ViewBag.Title = $"{player.Username} Info";
            return View();
        }
    }
}